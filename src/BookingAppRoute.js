"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
const ExpressRouter_1 = require("sunset-express-rc/lib/ExpressRouter");
const cors = require("cors");
const InquiresController_1 = require("./service/inquires/InquiresController");
const CustomerController_1 = require("./service/customers/CustomerController");
const AuthController_1 = require("@juicecommerce/juicecommerce/lib/modules/management/AuthController");
const UserController_1 = require("./service/users/UserController");
const AssetsController_1 = require("./service/assets/AssetsController");
class BookingAppRoute {
    constructor() {
        this.router = new ExpressRouter_1.ExpressRouter();
        this.router.use(cors());
        this.router.use(function (req, res, next) {
            // console.log(req.originalUrl);
            //return res.send({success:true});
            next();
        });
    }
    getRouter() {
        return this.router;
    }
    async init() {
        // this.testController = new TestController();
        // this.authController= new AuthController();
        this.inquiresController = new InquiresController_1.InquiresController();
        this.customerController = new CustomerController_1.CustomerController();
        this.userController = new UserController_1.UserController();
        this.assetController = new AssetsController_1.AssetController();
        // this.router.addController("/test", this.testController);
        this.router.addController("/inquires", this.inquiresController);
        this.router.addController("/customers", this.customerController);
        this.router.addController('/', new AuthController_1.AuthController());
        this.router.addController('/users', this.userController);
        this.router.addController('/assets', this.assetController);
        return true;
    }
}
__decorate([
    Inject_1.Inject("juicecommerce:networking")
], BookingAppRoute.prototype, "networking", void 0);
exports.BookingAppRoute = BookingAppRoute;
//# sourceMappingURL=BookingAppRoute.js.map