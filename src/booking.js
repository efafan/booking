"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ApplicationConfiguration_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/ApplicationConfiguration");
const ColoredConsoleLogger_1 = require("@juicecommerce/juicecommerce/lib/core/log/ColoredConsoleLogger");
const Juice_1 = require("@juicecommerce/juicecommerce/lib/Juice");
const NetworkingService_1 = require("@juicecommerce/juicecommerce/lib/services/networking/NetworkingService");
const BookingAppRoute_1 = require("./BookingAppRoute");
const JuicechainService_1 = require("./service/juicechain/JuicechainService");
const AssetService_1 = require("./service/assets/AssetService");
let booking = class booking {
    constructor() {
    }
    async configure() {
        return undefined;
    }
    async prepare() {
        Juice_1.Juice.register(ColoredConsoleLogger_1.ColoredConsoleLogger);
        await Juice_1.Juice.deployService(NetworkingService_1.NetworkingService);
        await Juice_1.Juice.deployService(JuicechainService_1.JuicechainService);
        await Juice_1.Juice.deployService(AssetService_1.AssetService);
        return undefined;
    }
    async ready() {
        let networking = Juice_1.Juice.service("juicecommerce:networking");
        //booking route
        let bookingApp = new BookingAppRoute_1.BookingAppRoute();
        await networking.deployRoute("/booking", bookingApp);
        console.log('Ready');
    }
};
booking = __decorate([
    ApplicationConfiguration_1.ApplicationConfiguration({
        key: "juicecommerce:tgm",
        options: {
            logger: "logger:colored-console"
        }
    })
], booking);
exports.booking = booking;
//# sourceMappingURL=booking.js.map