import {Model, Collection} from "sunshine-dao/lib/Model";
import {ObjectID} from "bson";

@Collection("inquires")
export class Inquire extends Model{

    number_of_persons: number;

    startDate: string;
    endDate: string;
    customer_id:ObjectID;

    reservation_id:ObjectID;

    canceled: boolean;



}