"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ChannelController_1 = require("@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController");
const ExpressController_1 = require("sunset-express-rc/lib/ExpressController");
const Inquire_1 = require("./model/Inquire");
const mongodb_1 = require("mongodb");
class InquiresController extends ChannelController_1.ChannelController {
    constructor() {
        super();
    }
    async createRequest(req, res) {
        let data = req.body;
        let inquire = new Inquire_1.Inquire();
        inquire.startDate = data.startDate;
        inquire.endDate = data.endDate;
        inquire.number_of_persons = data.quantity;
        inquire.customer_id = mongodb_1.ObjectID.createFromHexString(data.customer_id);
        await inquire.save();
        return res.send({
            success: true,
            inquire: inquire
        });
    }
    async fetch(req, res) {
        Inquire_1.Inquire
            .find({})
            .toArray()
            .then(data => {
            Inquire_1.Inquire.find({}).count().then(_data => {
                res.send({
                    inquires: data,
                    count: _data
                });
            });
        });
    }
    async getInquiry(req, res) {
        let id = mongodb_1.ObjectID.createFromHexString(req.params.id);
        Inquire_1.Inquire
            .findOne({ _id: id })
            .then(data => {
            res.send({
                inquires: data
            });
        });
    }
    async getCustomerInquires(req, res) {
        let customer_id = mongodb_1.ObjectID.createFromHexString(req.params.customerid);
        Inquire_1.Inquire
            .find({ customer_id: customer_id })
            .toArray()
            .then(data => {
            res.send({
                inquiry: data
            });
        });
    }
    async updateRequest(req, res) {
        let data = req.body;
        let id = mongodb_1.ObjectID.createFromHexString(req.params.id);
        let inquire = await Inquire_1.Inquire.findOne({ _id: id });
        inquire.reservation_id = mongodb_1.ObjectID.createFromHexString(data.reservation_id);
        await inquire.save();
        return res.send({
            success: true,
            inquire: inquire
        });
    }
}
__decorate([
    ExpressController_1.Post('/create')
], InquiresController.prototype, "createRequest", null);
__decorate([
    ExpressController_1.Post('/fetch')
], InquiresController.prototype, "fetch", null);
__decorate([
    ExpressController_1.Get('/:id')
], InquiresController.prototype, "getInquiry", null);
__decorate([
    ExpressController_1.Get('/customer/:customerid')
], InquiresController.prototype, "getCustomerInquires", null);
__decorate([
    ExpressController_1.Put('/update/:id')
], InquiresController.prototype, "updateRequest", null);
exports.InquiresController = InquiresController;
//# sourceMappingURL=InquiresController.js.map