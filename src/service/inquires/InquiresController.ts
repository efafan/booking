import {ChannelController} from "@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController";
import {Get, Post, Put} from "sunset-express-rc/lib/ExpressController";
import {Inquire} from "./model/Inquire";
import {ObjectID} from "mongodb";

export class InquiresController extends ChannelController {


    constructor() {
        super();
    }

    @Post('/create')
    async createRequest(req, res) {
        let data = req.body;
        let inquire = new Inquire();
        inquire.startDate=data.startDate;
        inquire.endDate=data.endDate;
        inquire.number_of_persons=data.quantity;
        inquire.customer_id=ObjectID.createFromHexString(data.customer_id);

        await inquire.save();

        return res.send({
            success: true,
            inquire: inquire
        });
    }

    @Post('/fetch')
    async fetch(req, res) {
        Inquire
            .find<Inquire>({})
            .toArray()
            .then(data => {
                Inquire.find({}).count().then(_data => {
                        res.send({
                            inquires: data,
                            count: _data
                        });
                });
            });
    }

    @Get('/:id')
    async getInquiry(req, res) {
        let id= ObjectID.createFromHexString(req.params.id);
        Inquire
            .findOne<Inquire>({_id:id})
            .then(data => {
                    res.send({
                        inquires: data
                    });
                });
    }

    @Get('/customer/:customerid')
    async getCustomerInquires(req, res) {
        let customer_id= ObjectID.createFromHexString(req.params.customerid);
        Inquire
            .find<Inquire>({customer_id:customer_id})
            .toArray()
            .then(data => {
                res.send({
                    inquiry: data
                });
            });
    }

    @Put('/update/:id')
    async updateRequest(req, res) {
        let data = req.body;
        let id= ObjectID.createFromHexString(req.params.id);
        let inquire = await Inquire.findOne<Inquire>({_id:id});
        inquire.reservation_id=ObjectID.createFromHexString(data.reservation_id);

        await inquire.save();

        return res.send({
            success: true,
            inquire: inquire
        });
    }

}