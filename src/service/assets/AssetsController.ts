import {ObjectID} from "mongodb";
import {Get, Post, Put} from "sunset-express-rc/lib/ExpressController";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {JuicechainService} from "../juicechain/JuicechainService";
import {AssetService} from "./AssetService";
import {AuthenticatedRequest} from "@juicecommerce/juicecommerce/lib/services/networking/filter/AuthenticatedRequest";
import {User} from "../users/models/User";
import {Asset} from "./models/Asset";
import {Commerce} from "@juicecommerce/juicecommerce/lib/services/commerce/models/Commerce";
import {ChannelController} from "@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController";
import {UserSessionFilter} from "@juicecommerce/juicecommerce/lib/services/juicebox/UserSessionFilter";
import {Customer} from "../customers/model/Customer";


export class AssetController extends ChannelController {

    @Inject("juicecommerce:juicechain")
    private juicechain: JuicechainService;


    @Inject("juicechain:assets")
    private assets: AssetService;




    constructor() {
        super();
    }


    /**
     * Get Node Infos
     */
    @Get('/node')
    private async getNodeInfo(req: AuthenticatedRequest, res){
        res.send(this.juicechain.getNodeInfo());
    }

    /**
     * Create Asset
     *
     * @param req
     * @param res
     */
    @Post("/:userId")
    private async createAsset(req, res){

        let data = req.body;
        let userId = ObjectID.createFromHexString(req.params.userId);

        const user = await User.findOne<User>({_id:userId});
        let waddress = user.wallet.address;

        let response = await this.assets.create(waddress, data.name, data.amount,
            data.publisher, data.type);
        if (response.success){

            // complete asset detials
            const asset = response.payload;
            asset.title = data.title;
            asset.created_id = user._id;
            asset.attributes = {};
            asset.validTill= data.validTill;
            asset.onDate= data.onDate;
            asset.endDate= data.endDate;
            asset.inquiry_id = ObjectID.createFromHexString(data.inquiry_id);

            if (data.inquires_id)
                asset.attributes.inquires_id = ObjectID.createFromHexString(data.inquires_id);

            const pkey = await this.juicechain.loadKey(waddress, "user", user._id);
            const auth = await this.juicechain.generateAuthorizationToken(pkey, waddress);
            await this.assets.updateText(auth, asset.name, data.title, null);


            await asset.save();

            return res.send({
                response:response,
                asset:asset
            });
        }

        res.send(response);
    }

    @Post("/sell/:userId")
    private async sellTicket(req: AuthenticatedRequest , res){

        let userId = ObjectID.createFromHexString(req.params.userId);

        const user = await User.findOne<User>({_id:userId});
        const originAddress = user.wallet.address;
        const pKey = await this.juicechain.loadKey(originAddress, "user", user._id);

        const receiver = await Customer.findOne<Customer>({
            _id: ObjectID.createFromHexString(req.body.entity_id)
        });

        let receiverAddress = null;
        if ((<any>receiver).wallets){
            receiverAddress = (<any>receiver).wallet.address;
        } else {
            let wallet = await this.juicechain.createWallet();
            (<any>receiver).wallets = [{ address: wallet.address, name: "direct-sales" }];
            receiverAddress = wallet.address;
            await this.juicechain.storeKey(wallet.address, wallet.privateKey, "entity", receiver._id);
            await receiver.save();
        }
        const asset = req.body.asset;
        const quantity = parseInt(req.body.quantity);

        let result = await this.juicechain.transfer(originAddress,
            pKey,
            receiverAddress,
            asset,
            quantity,
            req.body.payload);

        if (result.success) {
            let _asset = await Asset.findOne<Asset>({ name: asset });


            await result.log.attachUser(user._id);
            delete result.log;
        }

        res.send(result);
    }


    /**
     * Transfer Asset
     *
     * @param req
     * @param res
     */
    @Post("/transfer")
    private async transfer(req: AuthenticatedRequest, res){

        const user = await req.getUser();
        const originAddress = (<any>user.model).wallets[0].address;
        const pKey = await this.juicechain.loadKey(originAddress, "user", user.model._id);

        const receiver = await User.findOne<User>({ _id: ObjectID.createFromHexString(req.body.user_id)});
        const receiverAddress = receiver.wallet.address;

        const asset = req.body.asset;

        const quantity = parseInt(req.body.quantity);

        let result = await this.juicechain.transfer(originAddress,
            pKey,
            receiverAddress,
            asset,
            quantity,
            req.body.payload);

        if (result.success){
            await result.log.attachUser(user._id);
            delete result.log;
        }

        res.send(result);
    }



    @Post("/fetch/:page/:pagesize")
    async fetch(req: AuthenticatedRequest, res) {

        let options = req.body;
        let filters = options.filters;
        let page = parseInt(req.params.page);
        let pageSize = parseInt(req.params.pagesize);

        let query = {};

        // filter events by assets the users owns
        const user = await req.getUser();
        const realAssets = [];
        for(let address of (<any>user.model).wallets){
            const privateKey = await this.juicechain.loadKey(address.address, "user", user.model._id);
            const _assets = await this.juicechain.getBalance(address.address, privateKey);
            for(let asset of _assets) {
                realAssets.push(asset);
            }
        }

        query = { $or : [ { name: { $in: realAssets.map(asset => asset.name) } }, { "created_id" : user._id } ] };

        if (filters && (filters.name || filters.title)){
            const filter:any = { $or : [] };
            if (filters.name){
                filter.$or.push({ name : new RegExp(filters.name, 'i') });
            }
            if (filters.title){
                filter.$or.push({  title: new RegExp(filters.title, 'i') });
            }

            query = { $and: [query, filter] }
        }
        const filter:any = { $or : [] };
        if (filters.archived)
            filter.$or.push( { archived : filters.archived} );
        if (!filters.archived)
            filter.$or.push( { archived : {"$ne" : true }} );
        query = { $and: [query, filter] };

        const _assets = await Asset
            .find<Asset>(query)
            .limit(pageSize)
            .skip(page * pageSize)
            .toArray();

        const count = await Asset.find(query).count();

        // populate references
        // for(let asset of _assets){
        //     if (asset.attributes.event_id){
        //         (<any>asset)._event = await Event.findOne<Event>({
        //             _id: asset.attributes.event_id }, { name: true, onDate: true });
        //     }
        // }

        res.sendWithToken({
            assets: _assets.map(_asset => {
                for(const asset of realAssets){
                    if (_asset.name == asset.name){
                        _asset.quantity = asset.quantity;
                    }
                }
                return _asset;
            }),
            count: count
        });

    }


    @Get('/:id')
    async getAsset(req, res) {

        const id = ObjectID.createFromHexString(req.params.id);
        const asset = await Asset.findOne<Asset>({_id: id});


        let _asset = await this.assets.get(asset.name);
        if (!_asset || !_asset.success){
            return res.sendStatus(404);
        }

        // if (asset.attributes.event_id){
        //     (<any>asset)._event = await Event.findOne<Event>({
        //         _id: asset.attributes.event_id }, { name: true, onDate: true });
        // }

        (<any>asset).style = (<any>_asset.payload).style;
        (<any>asset).description = (<any>_asset.payload).description;
        (<any>asset).options = (<any>_asset.payload).options;
        (<any>asset).logo = (<any>_asset.payload).logo;
        (<any>asset).image = (<any>_asset.payload).image;
        (<any>asset).card = (<any>_asset.payload).card;
        asset.publisher = _asset.payload.publisher;
        asset.title = _asset.payload.title;
        (<any>asset).mediaURL = (<any>_asset.payload).mediaURL;
        (<any>asset).mediaStyle = (<any>_asset.payload).mediaStyle;
        (<any>asset).mediaType = (<any>_asset.payload).mediaType;

        return res.send(asset.toJSON(true));
    }


    @Get("/inquires/:id")
    public async getInquiryAsset(req, res){

        const asset = await Asset.find<Asset>({ "inquiry_id": ObjectID.createFromHexString(req.params.id) }).toArray();


        res.send({
            success:'true',
            asset: asset
        })
    }






    /**
     * Fetch assets sold to a customer
     *
     * @param req
     * @param res
     */
    @Get("/customer/:customer_id")
    public async getCustomerAssets(req, res){

        const customer_id = ObjectID.createFromHexString(req.params.customer_id);
        let orders = await Commerce.find<Commerce>({ customer_id : customer_id }).toArray();

        let assets = [];
        for(let order of orders){
            for (let item of order.items) {
                if (item.attributes && item.attributes.asset_id){
                    let asset = await Asset.findOne<Asset>({ _id: item.attributes.asset_id }, {
                        title: true
                    });
                    assets.push({
                        title: (<any>asset.title),
                        quantity: item.quantity,
                        date: order.created
                    });
                }
            }
        }

        res.send(assets);
    }


    @Put("/description")
    public async updateDescription(req, res){

        const name = req.body.name;
        const asset = await Asset.findOne<Asset>({ name: name });

        const user = await req.getUser();

        if (!asset.created_id.equals(user.model._id)){
            return res.send(404);
        }

        let waddress = (<any>user.model).wallets[0].address;
        const pkey = await this.juicechain.loadKey(waddress, "user", user._id);
        const auth = await this.juicechain.generateAuthorizationToken(pkey, waddress);

        let result = await this.assets.updateDescription(auth, name, req.body.description);

        res.sendWithToken({
            success: true
        })
    }

    @Get("/entity/:customer_id")
    public async getEntityAssets(req, res){

        const customer_id = ObjectID.createFromHexString(req.params.customer_id);
        const customer = await Customer.findOne<Customer>({ _id: customer_id });

        const assets = await Asset.find<Asset>({}).toArray();
        const tickets = [];

        if (customer.wallet) {
                for (let asset of assets) {
                    let balance = await this.juicechain.getAssetBalance(( <any>customer).wallet.address, "", asset.name);
                    if (balance > 0){
                        tickets.push({
                            title: asset.title,
                            quantity: balance
                        });
                    }
                }
        }

        res.send(tickets);
    }




    @Put("/:id")
    async update(req, res) {

        const ticketId = req.params.id;
        const data = req.body;

        const id = ObjectID.createFromHexString(ticketId);

        const ticket = await Asset.findOne<Asset>({_id:id});

        ticket.title=data.title;
        ticket.validTill=data.validTill;

        if (data.event_id){
            ticket.attributes.event_id = ObjectID.createFromHexString(data.event_id);
        }

        await ticket.save();

        const wallet = await this.getUserWallet(req);
        const privateKey = await this.juicechain.loadKey(wallet, "user", req.session.model.userId);
        const authorization = await this.juicechain.generateAuthorizationToken(privateKey, wallet);

        await this.assets.updateText(authorization, ticket.name,
            ticket.title, null);

        return res.send({
            success: true
        })

    }


    private async getUserWallet(req: AuthenticatedRequest): Promise<string>{
        const user = await req.getUser();
        return ((<any>user).model.wallets) ? (<any>user).model.wallets[0].address : null;
    }


}
