"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const ExpressController_1 = require("sunset-express-rc/lib/ExpressController");
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
const User_1 = require("../users/models/User");
const Asset_1 = require("./models/Asset");
const Commerce_1 = require("@juicecommerce/juicecommerce/lib/services/commerce/models/Commerce");
const ChannelController_1 = require("@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController");
const Customer_1 = require("../customers/model/Customer");
class AssetController extends ChannelController_1.ChannelController {
    constructor() {
        super();
    }
    /**
     * Get Node Infos
     */
    async getNodeInfo(req, res) {
        res.send(this.juicechain.getNodeInfo());
    }
    /**
     * Create Asset
     *
     * @param req
     * @param res
     */
    async createAsset(req, res) {
        let data = req.body;
        let userId = mongodb_1.ObjectID.createFromHexString(req.params.userId);
        const user = await User_1.User.findOne({ _id: userId });
        let waddress = user.wallet.address;
        let response = await this.assets.create(waddress, data.name, data.amount, data.publisher, data.type);
        if (response.success) {
            // complete asset detials
            const asset = response.payload;
            asset.title = data.title;
            asset.created_id = user._id;
            asset.attributes = {};
            asset.validTill = data.validTill;
            asset.onDate = data.onDate;
            asset.endDate = data.endDate;
            asset.inquiry_id = mongodb_1.ObjectID.createFromHexString(data.inquiry_id);
            if (data.inquires_id)
                asset.attributes.inquires_id = mongodb_1.ObjectID.createFromHexString(data.inquires_id);
            const pkey = await this.juicechain.loadKey(waddress, "user", user._id);
            const auth = await this.juicechain.generateAuthorizationToken(pkey, waddress);
            await this.assets.updateText(auth, asset.name, data.title, null);
            await asset.save();
            return res.send({
                response: response,
                asset: asset
            });
        }
        res.send(response);
    }
    async sellTicket(req, res) {
        let userId = mongodb_1.ObjectID.createFromHexString(req.params.userId);
        const user = await User_1.User.findOne({ _id: userId });
        const originAddress = user.wallet.address;
        const pKey = await this.juicechain.loadKey(originAddress, "user", user._id);
        const receiver = await Customer_1.Customer.findOne({
            _id: mongodb_1.ObjectID.createFromHexString(req.body.entity_id)
        });
        let receiverAddress = null;
        if (receiver.wallets) {
            receiverAddress = receiver.wallet.address;
        }
        else {
            let wallet = await this.juicechain.createWallet();
            receiver.wallets = [{ address: wallet.address, name: "direct-sales" }];
            receiverAddress = wallet.address;
            await this.juicechain.storeKey(wallet.address, wallet.privateKey, "entity", receiver._id);
            await receiver.save();
        }
        const asset = req.body.asset;
        const quantity = parseInt(req.body.quantity);
        let result = await this.juicechain.transfer(originAddress, pKey, receiverAddress, asset, quantity, req.body.payload);
        if (result.success) {
            let _asset = await Asset_1.Asset.findOne({ name: asset });
            await result.log.attachUser(user._id);
            delete result.log;
        }
        res.send(result);
    }
    /**
     * Transfer Asset
     *
     * @param req
     * @param res
     */
    async transfer(req, res) {
        const user = await req.getUser();
        const originAddress = user.model.wallets[0].address;
        const pKey = await this.juicechain.loadKey(originAddress, "user", user.model._id);
        const receiver = await User_1.User.findOne({ _id: mongodb_1.ObjectID.createFromHexString(req.body.user_id) });
        const receiverAddress = receiver.wallet.address;
        const asset = req.body.asset;
        const quantity = parseInt(req.body.quantity);
        let result = await this.juicechain.transfer(originAddress, pKey, receiverAddress, asset, quantity, req.body.payload);
        if (result.success) {
            await result.log.attachUser(user._id);
            delete result.log;
        }
        res.send(result);
    }
    async fetch(req, res) {
        let options = req.body;
        let filters = options.filters;
        let page = parseInt(req.params.page);
        let pageSize = parseInt(req.params.pagesize);
        let query = {};
        // filter events by assets the users owns
        const user = await req.getUser();
        const realAssets = [];
        for (let address of user.model.wallets) {
            const privateKey = await this.juicechain.loadKey(address.address, "user", user.model._id);
            const _assets = await this.juicechain.getBalance(address.address, privateKey);
            for (let asset of _assets) {
                realAssets.push(asset);
            }
        }
        query = { $or: [{ name: { $in: realAssets.map(asset => asset.name) } }, { "created_id": user._id }] };
        if (filters && (filters.name || filters.title)) {
            const filter = { $or: [] };
            if (filters.name) {
                filter.$or.push({ name: new RegExp(filters.name, 'i') });
            }
            if (filters.title) {
                filter.$or.push({ title: new RegExp(filters.title, 'i') });
            }
            query = { $and: [query, filter] };
        }
        const filter = { $or: [] };
        if (filters.archived)
            filter.$or.push({ archived: filters.archived });
        if (!filters.archived)
            filter.$or.push({ archived: { "$ne": true } });
        query = { $and: [query, filter] };
        const _assets = await Asset_1.Asset
            .find(query)
            .limit(pageSize)
            .skip(page * pageSize)
            .toArray();
        const count = await Asset_1.Asset.find(query).count();
        // populate references
        // for(let asset of _assets){
        //     if (asset.attributes.event_id){
        //         (<any>asset)._event = await Event.findOne<Event>({
        //             _id: asset.attributes.event_id }, { name: true, onDate: true });
        //     }
        // }
        res.sendWithToken({
            assets: _assets.map(_asset => {
                for (const asset of realAssets) {
                    if (_asset.name == asset.name) {
                        _asset.quantity = asset.quantity;
                    }
                }
                return _asset;
            }),
            count: count
        });
    }
    async getAsset(req, res) {
        const id = mongodb_1.ObjectID.createFromHexString(req.params.id);
        const asset = await Asset_1.Asset.findOne({ _id: id });
        let _asset = await this.assets.get(asset.name);
        if (!_asset || !_asset.success) {
            return res.sendStatus(404);
        }
        // if (asset.attributes.event_id){
        //     (<any>asset)._event = await Event.findOne<Event>({
        //         _id: asset.attributes.event_id }, { name: true, onDate: true });
        // }
        asset.style = _asset.payload.style;
        asset.description = _asset.payload.description;
        asset.options = _asset.payload.options;
        asset.logo = _asset.payload.logo;
        asset.image = _asset.payload.image;
        asset.card = _asset.payload.card;
        asset.publisher = _asset.payload.publisher;
        asset.title = _asset.payload.title;
        asset.mediaURL = _asset.payload.mediaURL;
        asset.mediaStyle = _asset.payload.mediaStyle;
        asset.mediaType = _asset.payload.mediaType;
        return res.send(asset.toJSON(true));
    }
    async getInquiryAsset(req, res) {
        const asset = await Asset_1.Asset.find({ "inquiry_id": mongodb_1.ObjectID.createFromHexString(req.params.id) }).toArray();
        res.send({
            success: 'true',
            asset: asset
        });
    }
    /**
     * Fetch assets sold to a customer
     *
     * @param req
     * @param res
     */
    async getCustomerAssets(req, res) {
        const customer_id = mongodb_1.ObjectID.createFromHexString(req.params.customer_id);
        let orders = await Commerce_1.Commerce.find({ customer_id: customer_id }).toArray();
        let assets = [];
        for (let order of orders) {
            for (let item of order.items) {
                if (item.attributes && item.attributes.asset_id) {
                    let asset = await Asset_1.Asset.findOne({ _id: item.attributes.asset_id }, {
                        title: true
                    });
                    assets.push({
                        title: asset.title,
                        quantity: item.quantity,
                        date: order.created
                    });
                }
            }
        }
        res.send(assets);
    }
    async updateDescription(req, res) {
        const name = req.body.name;
        const asset = await Asset_1.Asset.findOne({ name: name });
        const user = await req.getUser();
        if (!asset.created_id.equals(user.model._id)) {
            return res.send(404);
        }
        let waddress = user.model.wallets[0].address;
        const pkey = await this.juicechain.loadKey(waddress, "user", user._id);
        const auth = await this.juicechain.generateAuthorizationToken(pkey, waddress);
        let result = await this.assets.updateDescription(auth, name, req.body.description);
        res.sendWithToken({
            success: true
        });
    }
    async getEntityAssets(req, res) {
        const customer_id = mongodb_1.ObjectID.createFromHexString(req.params.customer_id);
        const customer = await Customer_1.Customer.findOne({ _id: customer_id });
        const assets = await Asset_1.Asset.find({}).toArray();
        const tickets = [];
        if (customer.wallet) {
            for (let asset of assets) {
                let balance = await this.juicechain.getAssetBalance(customer.wallet.address, "", asset.name);
                if (balance > 0) {
                    tickets.push({
                        title: asset.title,
                        quantity: balance
                    });
                }
            }
        }
        res.send(tickets);
    }
    async update(req, res) {
        const ticketId = req.params.id;
        const data = req.body;
        const id = mongodb_1.ObjectID.createFromHexString(ticketId);
        const ticket = await Asset_1.Asset.findOne({ _id: id });
        ticket.title = data.title;
        ticket.validTill = data.validTill;
        if (data.event_id) {
            ticket.attributes.event_id = mongodb_1.ObjectID.createFromHexString(data.event_id);
        }
        await ticket.save();
        const wallet = await this.getUserWallet(req);
        const privateKey = await this.juicechain.loadKey(wallet, "user", req.session.model.userId);
        const authorization = await this.juicechain.generateAuthorizationToken(privateKey, wallet);
        await this.assets.updateText(authorization, ticket.name, ticket.title, null);
        return res.send({
            success: true
        });
    }
    async getUserWallet(req) {
        const user = await req.getUser();
        return (user.model.wallets) ? user.model.wallets[0].address : null;
    }
}
__decorate([
    Inject_1.Inject("juicecommerce:juicechain")
], AssetController.prototype, "juicechain", void 0);
__decorate([
    Inject_1.Inject("juicechain:assets")
], AssetController.prototype, "assets", void 0);
__decorate([
    ExpressController_1.Get('/node')
], AssetController.prototype, "getNodeInfo", null);
__decorate([
    ExpressController_1.Post("/:userId")
], AssetController.prototype, "createAsset", null);
__decorate([
    ExpressController_1.Post("/sell/:userId")
], AssetController.prototype, "sellTicket", null);
__decorate([
    ExpressController_1.Post("/transfer")
], AssetController.prototype, "transfer", null);
__decorate([
    ExpressController_1.Post("/fetch/:page/:pagesize")
], AssetController.prototype, "fetch", null);
__decorate([
    ExpressController_1.Get('/:id')
], AssetController.prototype, "getAsset", null);
__decorate([
    ExpressController_1.Get("/inquires/:id")
], AssetController.prototype, "getInquiryAsset", null);
__decorate([
    ExpressController_1.Get("/customer/:customer_id")
], AssetController.prototype, "getCustomerAssets", null);
__decorate([
    ExpressController_1.Put("/description")
], AssetController.prototype, "updateDescription", null);
__decorate([
    ExpressController_1.Get("/entity/:customer_id")
], AssetController.prototype, "getEntityAssets", null);
__decorate([
    ExpressController_1.Put("/:id")
], AssetController.prototype, "update", null);
exports.AssetController = AssetController;
//# sourceMappingURL=AssetsController.js.map