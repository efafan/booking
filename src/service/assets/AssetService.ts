import {ServiceConfiguration} from "@juicecommerce/juicecommerce/lib/core/decorators/ServiceConfiguration";
import {IService} from "@juicecommerce/juicecommerce/lib/core/service/IService";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {JuicechainService} from "../juicechain/JuicechainService";
import {Result} from "@juicecommerce/juicecommerce/lib/core/Result";
import {Asset} from "./models/Asset";


var rp = require('request-promise');

@ServiceConfiguration({
    key: "juicechain:assets",
    options: {
        prefix: ""
    }
})
export class AssetService implements IService{

    options: any;

    @Inject("juicecommerce:juicechain")
    private juicechain: JuicechainService;

    constructor() {}

    /**
     * Create new asset
     *
     * @returns {Promise<ManagedAsset>}
     * TODO Implement JWT authorization
     */
    async create(address: string, name: string, amount: number,
                 publisher: string, type: string): Promise<Result<Asset>> {

        const token = await this.juicechain.getJWT();
        const _amount = parseInt(<any>amount);

        let _name = name;
        if (this.options.prefix){
            _name = this.options.prefix + ":" + name;
        }

        const options = {
            method: 'POST',
            uri: this.juicechain.getNodeUrl() + "/assets/",
            headers: {
                "authorization": token
            },
            body: {
                title: { de_DE: name },
                name: _name,
                type: type,
                amount: _amount,
                target: address,
                publisher: publisher,
                options: {
                    transferAll: true,
                    transferNode: true,
                    returnAddress: null
                }
            },
            json: true
        };

        try {
            let result = await rp(options);
            if (result.success){
                let asset = new Asset();
                asset.__elevate(result.payload);
                return {
                    success: true,
                    payload: asset
                }
            } else {
                if (typeof result.error === 'string'){
                    return {
                        success: false,
                        error: result.error
                    }
                } else {
                    return {
                        success: false,
                        error: result.error.message
                    }
                }
            }
        } catch (exception){
            return exception;
        }

    }

    /**
     * Fetch asset details from issuing node
     *
     * @param name
     * TODO implement JWT authorization
     */
    public async get(name: string):Promise<Result<Asset>>{

        const token = await this.juicechain.getJWT();

        const options = {
            method: 'POST',
            uri: this.juicechain.getNodeUrl() + "/assets/details",
            headers: {
                "authorization": token
            },
            body: {
                name: name,
            },
            json: true
        };

        try {
            let result = await rp(options);
            if (result.success){
                return {
                    success: true,
                    payload: result.payload
                }
            } else {
                return {
                    success: false,
                    error: "Request failed"
                }
            }
        } catch (exception){
            return exception;
        }

    }


    /**
     * Update assets description
     * @param authorization
     * @param asset
     * @param description
     * TODO implement JWT authorization
     */
    async updateDescription(authorization: string, asset: string, description: string): Promise<boolean>{

        const token = await this.juicechain.getJWT();

        const options = {
            method: 'PUT',
            uri: this.juicechain.getNodeUrl() + "/assets/description",
            headers: {
                "authorization": token
            },
            body: {
                name: asset,
                description: description
            },
            json: true
        };

        try {
            let result = await rp(options);
            return result;
        } catch (exception){
            return exception;
        }

    }



    /**
     * Update asset title and description
     *
     * @param authorization
     * @param asset
     * @param title
     * @param description
     * TODO implement JWT authorization
     */
    async updateText(authorization: string, asset: string, title: any, description: any):Promise<boolean>{

        const token = await this.juicechain.getJWT();

        const options = {
            method: 'PUT',
            uri: this.juicechain.getNodeUrl() + "/assets/title",
            headers: {
                authorization: token
            },
            body: {
                name: asset,
                title: title,
                description: description
            },
            json: true
        };

        try {
            let result = await rp(options);
            return result;
        } catch (exception){
            return exception;
        }

    }

    async onUpdate(options: any): Promise<boolean> {
        this.options = options;
        return true;
    }

    onConfigure(): Promise<boolean> { return undefined; }
    onShutdown(): Promise<boolean> { return undefined; }
    onStartup(): Promise<boolean> { return undefined; }

}
