"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ServiceConfiguration_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/ServiceConfiguration");
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
const Asset_1 = require("./models/Asset");
var rp = require('request-promise');
let AssetService = class AssetService {
    constructor() { }
    /**
     * Create new asset
     *
     * @returns {Promise<ManagedAsset>}
     * TODO Implement JWT authorization
     */
    async create(address, name, amount, publisher, type) {
        const token = await this.juicechain.getJWT();
        const _amount = parseInt(amount);
        let _name = name;
        if (this.options.prefix) {
            _name = this.options.prefix + ":" + name;
        }
        const options = {
            method: 'POST',
            uri: this.juicechain.getNodeUrl() + "/assets/",
            headers: {
                "authorization": token
            },
            body: {
                title: { de_DE: name },
                name: _name,
                type: type,
                amount: _amount,
                target: address,
                publisher: publisher,
                options: {
                    transferAll: true,
                    transferNode: true,
                    returnAddress: null
                }
            },
            json: true
        };
        try {
            let result = await rp(options);
            if (result.success) {
                let asset = new Asset_1.Asset();
                asset.__elevate(result.payload);
                return {
                    success: true,
                    payload: asset
                };
            }
            else {
                if (typeof result.error === 'string') {
                    return {
                        success: false,
                        error: result.error
                    };
                }
                else {
                    return {
                        success: false,
                        error: result.error.message
                    };
                }
            }
        }
        catch (exception) {
            return exception;
        }
    }
    /**
     * Fetch asset details from issuing node
     *
     * @param name
     * TODO implement JWT authorization
     */
    async get(name) {
        const token = await this.juicechain.getJWT();
        const options = {
            method: 'POST',
            uri: this.juicechain.getNodeUrl() + "/assets/details",
            headers: {
                "authorization": token
            },
            body: {
                name: name,
            },
            json: true
        };
        try {
            let result = await rp(options);
            if (result.success) {
                return {
                    success: true,
                    payload: result.payload
                };
            }
            else {
                return {
                    success: false,
                    error: "Request failed"
                };
            }
        }
        catch (exception) {
            return exception;
        }
    }
    /**
     * Update assets description
     * @param authorization
     * @param asset
     * @param description
     * TODO implement JWT authorization
     */
    async updateDescription(authorization, asset, description) {
        const token = await this.juicechain.getJWT();
        const options = {
            method: 'PUT',
            uri: this.juicechain.getNodeUrl() + "/assets/description",
            headers: {
                "authorization": token
            },
            body: {
                name: asset,
                description: description
            },
            json: true
        };
        try {
            let result = await rp(options);
            return result;
        }
        catch (exception) {
            return exception;
        }
    }
    /**
     * Update asset title and description
     *
     * @param authorization
     * @param asset
     * @param title
     * @param description
     * TODO implement JWT authorization
     */
    async updateText(authorization, asset, title, description) {
        const token = await this.juicechain.getJWT();
        const options = {
            method: 'PUT',
            uri: this.juicechain.getNodeUrl() + "/assets/title",
            headers: {
                authorization: token
            },
            body: {
                name: asset,
                title: title,
                description: description
            },
            json: true
        };
        try {
            let result = await rp(options);
            return result;
        }
        catch (exception) {
            return exception;
        }
    }
    async onUpdate(options) {
        this.options = options;
        return true;
    }
    onConfigure() { return undefined; }
    onShutdown() { return undefined; }
    onStartup() { return undefined; }
};
__decorate([
    Inject_1.Inject("juicecommerce:juicechain")
], AssetService.prototype, "juicechain", void 0);
AssetService = __decorate([
    ServiceConfiguration_1.ServiceConfiguration({
        key: "juicechain:assets",
        options: {
            prefix: ""
        }
    })
], AssetService);
exports.AssetService = AssetService;
//# sourceMappingURL=AssetService.js.map