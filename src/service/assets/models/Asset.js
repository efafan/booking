"use strict";
/**
 * JuicEcommerce V1
 * Copyright (c) Michael Hasler
 *
 * This application can be used under under the terms of the
 * GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Model_1 = require("sunshine-dao/lib/Model");
/**
 * Schema for asset
 *
 * Asset is required for juicechain.
 *
 */
let Asset = class Asset extends Model_1.Model {
    constructor() {
        super();
        this.created = new Date();
        this.__updateOnSave = "updated";
    }
};
Asset = __decorate([
    Model_1.Collection("assets")
], Asset);
exports.Asset = Asset;
//# sourceMappingURL=Asset.js.map