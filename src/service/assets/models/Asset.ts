/**
 * JuicEcommerce V1
 * Copyright (c) Michael Hasler
 *
 * This application can be used under under the terms of the
 * GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */

import {Collection, Model} from "sunshine-dao/lib/Model";
import {ObjectID} from "mongodb";

/**
 * Schema for asset
 *
 * Asset is required for juicechain.
 *
 */

@Collection("assets")
export class Asset extends Model{

    created: Date;
    updated: Date;

    constructor(){
        super();
        this.created = new Date();
        this.__updateOnSave = "updated";
    }

    // asset name BC
    name: string;

    title: string | Object;
    description: string | Object;

    type: string;

    publisher:string;

    // initial issued amount
    amount: number;
    archived: boolean;
    inquiry_id: ObjectID;

    // currently available amount
    quantity: number;

    // user issued this asset
    created_id: ObjectID;

    attributes: any;

    validTill: string;
    onDate: string;
    endDate: string;

    prices: [{
        term: string;
        net: number;
    }];


}
