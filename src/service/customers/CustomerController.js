"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ChannelController_1 = require("@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController");
const ExpressController_1 = require("sunset-express-rc/lib/ExpressController");
const Customer_1 = require("./model/Customer");
const mongodb_1 = require("mongodb");
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
class CustomerController extends ChannelController_1.ChannelController {
    constructor() {
        super();
    }
    async createCustomer(req, res) {
        let data = req.body;
        let wallet = await this.juicechain.createWallet();
        let customer = new Customer_1.Customer();
        customer.firstname = data.firstname;
        customer.lastname = data.lastname;
        customer.email = data.email;
        if (!customer.wallet)
            customer.wallet = {};
        customer.wallet = { address: wallet.address };
        await customer.save();
        await this.juicechain.storeKey(wallet.address, wallet.privateKey, "customer", customer._id);
        return res.send({
            success: true,
            customer: customer
        });
    }
    async createWallet(req, res) {
        let data = req.body;
        let customerId = req.params.id;
        const id = mongodb_1.ObjectID.createFromHexString(customerId);
        //res.send(data);
        if (!data.address) {
            let wallet = await this.juicechain.createWallet();
            data.address = wallet.address;
        }
        Customer_1.Customer.findOne({ _id: id }).then(customer => {
            if (!customer.wallet)
                customer.wallets = {};
            customer.wallet = data;
            customer.save().then(result => {
                res.send({ success: result, customer: customer });
            }).catch(err => {
                res.status(500).send(err);
            });
        });
    }
    async fetch(req, res) {
        Customer_1.Customer
            .find({})
            .toArray()
            .then(data => {
            Customer_1.Customer.find({}).count().then(_data => {
                res.send({
                    customers: data,
                    count: _data
                });
            });
        });
    }
}
__decorate([
    Inject_1.Inject("juicecommerce:juicechain")
], CustomerController.prototype, "juicechain", void 0);
__decorate([
    ExpressController_1.Post('/create')
], CustomerController.prototype, "createCustomer", null);
__decorate([
    ExpressController_1.Post('/wallets/:id')
], CustomerController.prototype, "createWallet", null);
__decorate([
    ExpressController_1.Post('/fetch')
], CustomerController.prototype, "fetch", null);
exports.CustomerController = CustomerController;
//# sourceMappingURL=CustomerController.js.map