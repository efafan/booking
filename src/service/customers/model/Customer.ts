import {Model, Collection} from "sunshine-dao/lib/Model";
import {ObjectID} from "bson";

@Collection("customers")
export class Customer extends Model{

    firstname: string;
    lastname: string;
    email: string;
    wallet: Object;




}