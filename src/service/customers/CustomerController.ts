import {ChannelController} from "@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController";
import {Get, Post} from "sunset-express-rc/lib/ExpressController";
import {Customer} from "./model/Customer";
import {ObjectID} from "mongodb";
import {JuicechainService} from "../juicechain/JuicechainService";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {Inquire} from "../inquires/model/Inquire";


export class CustomerController extends ChannelController {

    @Inject("juicecommerce:juicechain")
    private juicechain: JuicechainService;

    constructor() {
        super();
    }

    @Post('/create')
    async createCustomer(req, res) {
        let data = req.body;

        let wallet= await this.juicechain.createWallet();

        let customer = new Customer();
        customer.firstname=data.firstname;
        customer.lastname=data.lastname;
        customer.email=data.email;

        if (!customer.wallet) (<any>customer).wallet = {};
        customer.wallet={address:wallet.address};

        await customer.save();

        await this.juicechain.storeKey(wallet.address, wallet.privateKey, "customer", customer._id);

        return res.send({
            success: true,
            customer: customer
        });
    }

    @Post('/wallets/:id')
    async createWallet(req, res){

        let data = req.body;
        let customerId = req.params.id;
        const id = ObjectID.createFromHexString(customerId);
        //res.send(data);

        if (!data.address){
            let wallet= await this.juicechain.createWallet();
            data.address=wallet.address;
        }

        Customer.findOne<Customer>({_id: id}).then(customer=>
        {

            if (!customer.wallet) (<any>customer).wallets = {};
            customer.wallet=data;


            customer.save().then(result => {
                res.send({ success: result, customer: customer });
            }).catch(err => {
                res.status(500).send(err);
            });
        })


    }

    @Post('/fetch')
    async fetch(req, res) {
        Customer
            .find<Customer>({})
            .toArray()
            .then(data => {
                Customer.find({}).count().then(_data => {
                    res.send({
                        customers: data,
                        count: _data
                    });
                });
            });
    }

}