import {Model, Collection} from "sunshine-dao/lib/Model";
import {ObjectID} from "bson";

@Collection("users")
export class User extends Model{

    firstname: string;
    lastname: string;
    email: string;
    wallet: {
        address:string
    };
    password: string;
    roles:string;




}