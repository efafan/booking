"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ChannelController_1 = require("@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController");
const ExpressController_1 = require("sunset-express-rc/lib/ExpressController");
const mongodb_1 = require("mongodb");
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
const User_1 = require("./models/User");
const Customer_1 = require("../customers/model/Customer");
class UserController extends ChannelController_1.ChannelController {
    constructor() {
        super();
    }
    async getUser(req, res) {
        let id = req.params.id;
        User_1.User.findOne({ _id: mongodb_1.ObjectID.createFromHexString(id) }).then(user => {
            return res.send({
                success: true,
                user: user
            });
        });
    }
    async createUser(req, res) {
        let data = req.body;
        let user = await this.networking.createUser(data.email, data.password);
        if (!user || !user.success) {
            return res.sendWithToken({
                success: false,
                error: "Email is already taken"
            });
        }
        user = user.payload;
        await user.parse(data);
        user.model.organisation_id = (await req.getUser()).model.organisation_id;
        let wallet = await this.juicechain.createWallet();
        await this.juicechain.storeKey(wallet.address, wallet.privateKey, "user", user._id);
        if (!user.wallet)
            user.wallet = {};
        user.wallet = { address: wallet.address };
        await user.save();
        res.send({
            success: true
        });
    }
    async createUserFromCustomer(req, res) {
        let data = req.body;
        let user = await this.networking.createUser(data.email, data.password);
        if (!user) {
            return res.send({
                success: false,
                error: "Email is already taken"
            });
        }
        user = user.model;
        user.firstname = data.firstname;
        user.lastname = data.lastname;
        user.customer_id = mongodb_1.ObjectID.createFromHexString(data.customer_id);
        user.roles = 'customer';
        Customer_1.Customer.findOne({ _id: user.customer_id }).then(async (customer) => {
            if (customer.wallet) {
                if (!user.wallet)
                    user.wallet = {};
                user.wallet.address = customer.wallet.address;
                await user.save();
            }
        });
        await user.save();
        res.send({
            success: true
        });
    }
}
__decorate([
    Inject_1.Inject("juicecommerce:juicechain")
], UserController.prototype, "juicechain", void 0);
__decorate([
    Inject_1.Inject("juicecommerce:networking")
], UserController.prototype, "networking", void 0);
__decorate([
    ExpressController_1.Get('/:id')
], UserController.prototype, "getUser", null);
__decorate([
    ExpressController_1.Post('/')
], UserController.prototype, "createUser", null);
__decorate([
    ExpressController_1.Post('/customer/create')
], UserController.prototype, "createUserFromCustomer", null);
exports.UserController = UserController;
//# sourceMappingURL=UserController.js.map