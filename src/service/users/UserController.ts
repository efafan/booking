import {ChannelController} from "@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController";
import {Get, Post} from "sunset-express-rc/lib/ExpressController";
import {ObjectID} from "mongodb";
import {JuicechainService} from "../juicechain/JuicechainService";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {User} from "./models/User";
import {NetworkingService} from "@juicecommerce/juicecommerce/lib/services/networking/NetworkingService";
import {Customer} from "../customers/model/Customer";
import {UserSessionFilter} from "@juicecommerce/juicecommerce/lib/services/juicebox/UserSessionFilter";


export class UserController extends ChannelController {

    @Inject("juicecommerce:juicechain")
    private juicechain: JuicechainService;

    @Inject("juicecommerce:networking")
    private networking: NetworkingService;

    constructor() {
        super();
    }

    @Get('/:id')
    async getUser(req,res){
        let id = req.params.id;
        User.findOne<User>({_id:ObjectID.createFromHexString(id)}).then(user=>{
            return res.send({
                        success: true,
                        user: user
                    });
        })
    }

    @Post('/')
    async createUser(req, res) {

        let data = req.body;

        let user:any = await this.networking.createUser(data.email, data.password);
        if (!user || !user.success) {
            return res.sendWithToken({
                success: false,
                error: "Email is already taken"
            });
        }

        user = user.payload;
        await user.parse(data);
        user.model.organisation_id = (await req.getUser()).model.organisation_id;


        let wallet = await this.juicechain.createWallet();
        await this.juicechain.storeKey(wallet.address, wallet.privateKey, "user", user._id);
        if (!user.wallet) (<any>user).wallet = {};
        user.wallet={address:wallet.address};
        await user.save();


        res.send({
            success: true
        })
    }

    @Post('/customer/create')
    async createUserFromCustomer(req, res) {

        let data = req.body;

        let user:any = await this.networking.createUser(data.email, data.password);
        if (!user ) {
            return res.send({
                success: false,
                error: "Email is already taken"
            });
        }

        user = user.model;
        user.firstname=data.firstname;
        user.lastname=data.lastname;
        user.customer_id= ObjectID.createFromHexString(data.customer_id);
        user.roles='customer';

        Customer.findOne<Customer>({_id: user.customer_id}).then(async customer=>{
            if (customer.wallet){
                if (!user.wallet) (<any>user).wallet = {};
                user.wallet.address=(<any>customer).wallet.address;
                await user.save();
            }
        });
        await user.save();


        res.send({
            success: true
        })
    }
    // @Post('/create')
    // async createCustomer(req, res) {
    //     let data = req.body;
    //
    //     let wallet= await this.juicechain.createWallet();
    //
    //     let customer = new Customer();
    //     customer.firstname=data.firstname;
    //     customer.lastname=data.lastname;
    //     customer.email=data.email;
    //
    //     if (!customer.wallet) (<any>customer).wallet = {};
    //     customer.wallet={address:wallet.address};
    //
    //     await customer.save();
    //
    //     await this.juicechain.storeKey(wallet.address, wallet.privateKey, "customer", customer._id);
    //
    //     return res.send({
    //         success: true,
    //         customer: customer
    //     });
    // }
    //
    // @Post('/wallets/:id')
    // async createWallet(req, res){
    //
    //     let data = req.body;
    //     let customerId = req.params.id;
    //     const id = ObjectID.createFromHexString(customerId);
    //     //res.send(data);
    //
    //     if (!data.address){
    //         let wallet= await this.juicechain.createWallet();
    //         data.address=wallet.address;
    //     }
    //
    //     Customer.findOne<Customer>({_id: id}).then(customer=>
    //     {
    //
    //         if (!customer.wallet) (<any>customer).wallets = {};
    //         customer.wallet=data;
    //
    //
    //         customer.save().then(result => {
    //             res.send({ success: result, customer: customer });
    //         }).catch(err => {
    //             res.status(500).send(err);
    //         });
    //     })
    //
    //
    // }

}