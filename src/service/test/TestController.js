"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ChannelController_1 = require("@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController");
const ExpressController_1 = require("sunset-express-rc/lib/ExpressController");
class TestController extends ChannelController_1.ChannelController {
    constructor() {
        super();
    }
    async getInfo(req, res) {
        let string = "this is test";
        return res.send({
            string: string
        });
    }
}
__decorate([
    ExpressController_1.Get('/test')
], TestController.prototype, "getInfo", null);
exports.TestController = TestController;
//# sourceMappingURL=TestController.js.map