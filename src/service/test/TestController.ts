import {ChannelController} from "@juicecommerce/juicecommerce/lib/services/networking/channel/ChannelController";
import {Get} from "sunset-express-rc/lib/ExpressController";

export class TestController extends ChannelController {


    constructor() {
        super();
    }

    @Get('/test')
    async getInfo(req, res) {
        let string= "this is test";

        return res.send({
            string: string
        });
    }

}