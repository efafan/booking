"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var TransferLog_1;
const Model_1 = require("sunshine-dao/lib/Model");
let TransferLog = TransferLog_1 = class TransferLog extends Model_1.Model {
    constructor() {
        super();
    }
    static async log(from, to, asset, quantity, payload, txId, success) {
        const log = new TransferLog_1();
        log.timestamp = new Date();
        log.from = from;
        log.to = to;
        log.asset = asset;
        log.quantity = quantity;
        log.payload = payload;
        log.txId = txId;
        log.success = success;
        await log.save();
        return log;
    }
    async attachUser(user_id) {
        this.user_id = user_id;
        await TransferLog_1.update({ _id: this._id }, {
            $set: {
                user_id: user_id
            }
        });
    }
};
TransferLog = TransferLog_1 = __decorate([
    Model_1.Collection("transfers")
], TransferLog);
exports.TransferLog = TransferLog;
//# sourceMappingURL=TransferLog.js.map