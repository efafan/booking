import {Collection, Model} from "sunshine-dao/lib/Model";
import {ObjectID} from "bson";

@Collection("keystore")
export class KeyStore extends Model{

    constructor() {
        super();
        if (!this.timestamp)
            this.timestamp = new Date();
    }

    address: string;

    privateKey: string;
    publicKey: string;

    reference: string;
    reference_id: ObjectID;

    timestamp: Date;

}