import {Model, Collection} from "sunshine-dao/lib/Model";
import {ObjectId, ObjectID} from "bson";

@Collection("transfers")
export class TransferLog extends Model{

    constructor() {
        super();
    }

    timestamp: Date;
    user_id: ObjectID;
    from: string;
    to: string;
    asset: string;
    quantity: number;
    payload: any;
    success: boolean;
    txId: string;

    static async log(from, to, asset, quantity, payload, txId, success?):Promise<TransferLog>{
        const log = new TransferLog();
        log.timestamp = new Date();
        log.from = from;
        log.to = to;
        log.asset = asset;
        log.quantity = quantity;
        log.payload = payload;
        log.txId = txId;
        log.success = success;
        await log.save();

        return log;
    }

    async attachUser(user_id: ObjectId): Promise<any>{
        this.user_id = user_id;
        await TransferLog.update({ _id: this._id }, {
                $set: {
                    user_id: user_id
                }
            }
        );
    }

}