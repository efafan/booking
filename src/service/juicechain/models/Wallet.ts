export class Wallet{

    address: string;
    privateKey: string;
    publicKey: string;
    node: string;
    provider: string;

}