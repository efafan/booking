"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ServiceConfiguration_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/ServiceConfiguration");
const Inject_1 = require("@juicecommerce/juicecommerce/lib/core/decorators/Inject");
const KeyStore_1 = require("./models/KeyStore");
const CryptoUtils_1 = require("./tools/CryptoUtils");
const TransferLog_1 = require("./models/TransferLog");
let rp = require('request-promise');
let JuicechainService = class JuicechainService {
    constructor() { }
    /* async manage(element: ObjectId | Asset): Promise<ManagedAsset> {
         const managed = new ManagedAsset();
         await managed.load(element);
         return managed;
     }
     */
    getNodeInfo() {
        const name = this.options.juicechain.node;
        return {
            name: name
        };
    }
    async getJWT() {
        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + '/auth',
            headers: {
                'authorization': 'none'
            },
            body: {
                username: this.options.juicechain.user,
                password: this.options.juicechain.password
            },
            json: true
        };
        try {
            let result = await rp(options);
            if (result && result.success) {
                return result.token;
            }
            else
                return null;
        }
        catch (e) {
            return e;
        }
    }
    /**
     * Fetch assets of given wallet
     * @param address
     * @param privateKey
     * TODO Change authToken to JWT
     */
    async getBalance(address, privateKey) {
        //  const authToken = await CryptoUtils.generateAuthToken(privateKey, address);
        const token = await this.getJWT();
        const minconf = (this.options.juicechain.minconf != null) ? this.options.juicechain.minconf : 1;
        const options = {
            method: 'GET',
            uri: this.options.juicechain.url + "/wallet/" + address + "/" + minconf + "/" + (new Date()).toString(),
            headers: {
                "authorization": token
            },
            body: {},
            json: true
        };
        try {
            let result = await rp(options);
            if (result && result.success) {
                return result.payload;
            }
            else {
                return [];
            }
        }
        catch (exception) {
            return exception;
        }
    }
    /**
     * Transfer Asset
     *
     * @param senderAddress
     * @param senderPrivateKey
     * @param receiverAddress
     * @param asset
     * @param quantity
     * @param payload
     */
    async transfer(senderAddress, senderPrivateKey, receiverAddress, asset, quantity, payload) {
        const authToken = await CryptoUtils_1.CryptoUtils.generateAuthToken(senderPrivateKey, senderAddress);
        quantity = parseInt(quantity);
        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + "/wallet/transfer/" + receiverAddress,
            headers: {
                "authorization": authToken
            },
            body: {
                asset: asset,
                amount: quantity,
                payload: payload
            },
            json: true
        };
        try {
            let result = await rp(options);
            // generate log
            result.log = await TransferLog_1.TransferLog.log(senderAddress, receiverAddress, asset, quantity, payload, result.payload, result.success);
            return result;
        }
        catch (exception) {
            return exception;
        }
    }
    async generateAuthorizationToken(privateKey, publicKey) {
        return await CryptoUtils_1.CryptoUtils.generateAuthToken(privateKey, publicKey);
    }
    async loadKey(address, reference, reference_id) {
        const query = {
            address: address,
            reference: reference,
            reference_id: reference_id
        };
        const key = await KeyStore_1.KeyStore.findOne(query);
        return (key) ? key.privateKey : null;
    }
    /**TODO JWT authorization
     */
    async createWallet() {
        const token = await this.getJWT();
        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + "/wallet/",
            headers: {
                "authorization": token
            },
            body: {},
            json: true
        };
        try {
            let result = await rp(options);
            if (result && result.success) {
                return {
                    address: result.payload.address,
                    privateKey: result.payload.privateKey,
                    publicKey: result.payload.publicKey,
                    node: result.payload.node,
                    provider: result.payload.provider,
                    title: result.payload.title
                };
            }
            return null;
        }
        catch (exception) {
            return exception;
        }
    }
    async storeKey(address, privateKey, reference, reference_id) {
        const keyStore = new KeyStore_1.KeyStore();
        keyStore.address = address;
        keyStore.privateKey = privateKey;
        keyStore.reference = reference;
        keyStore.reference_id = reference_id;
        await keyStore.save();
        return true;
    }
    /**
     * Get specific amount of assets currently available in an wallet
     * @param address
     * @param assetName
     */
    async getAssetBalance(address, privateKey, assetName) {
        let balance = await this.getBalance(address, privateKey);
        let amount = 0;
        balance.forEach(asset => {
            if (asset.name == assetName) {
                amount = asset.quantity;
            }
        });
        return amount;
    }
    async onUpdate(options) {
        this.options = options;
        return true;
    }
    getNodeUrl() {
        return this.options.juicechain.url;
    }
    async onConfigure() {
        return undefined;
    }
    // TODO: Fetch token
    async onStartup() {
        this.logging.debug("JuicEchain", "Node: " + this.options.juicechain.node + " (" + this.options.juicechain.url + ")");
        return true;
    }
};
__decorate([
    Inject_1.Inject("juicecommerce:logging")
], JuicechainService.prototype, "logging", void 0);
JuicechainService = __decorate([
    ServiceConfiguration_1.ServiceConfiguration({
        name: "Juicechain",
        key: "juicecommerce:juicechain",
        description: "Service connecting Juicechain API",
        options: {
            juicechain: {
                node: "demo",
                url: "http://demo.juicechain.org",
                user: "none",
                password: "none"
            }
        }
    })
], JuicechainService);
exports.JuicechainService = JuicechainService;
//# sourceMappingURL=JuicechainService.js.map