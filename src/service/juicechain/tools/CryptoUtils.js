"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Message_1 = require("./Message");
const bitcore = require('bitcore-lib');
class CryptoUtils {
    /**
     * Generate short-live token based on private
     *
     * @param privateKey
     * @param device
     * @param asset
     * @param address
     * @returns {string}
     */
    static generateAuthToken(privateKey, address, device, asset) {
        const _privateKey = bitcore.PrivateKey.fromWIF(privateKey);
        const payload = {
            address: address,
            device: device,
            asset: asset,
            timestamp: new Date(),
        };
        // generate signature
        const message = new Message_1.Message(JSON.stringify(payload));
        const signature = message.sign(_privateKey);
        // generate message
        payload.signature = signature;
        return new Buffer(JSON.stringify(payload)).toString("base64");
    }
    ;
}
exports.CryptoUtils = CryptoUtils;
//# sourceMappingURL=CryptoUtils.js.map