import {Message} from "./Message";
import * as crypto from 'crypto';

const bitcore = require('bitcore-lib');

export class CryptoUtils{

    /**
     * Generate short-live token based on private
     *
     * @param privateKey
     * @param device
     * @param asset
     * @param address
     * @returns {string}
     */
    public static generateAuthToken(privateKey, address, device?: string, asset?: string): string {

        const _privateKey = bitcore.PrivateKey.fromWIF(privateKey);
        const payload:any = {
            address: address,
            device: device,
            asset: asset,
            timestamp: new Date(),
        };

        // generate signature
        const message = new Message(JSON.stringify(payload));
        const signature = message.sign(_privateKey);

        // generate message
        payload.signature = signature;

        return new Buffer(JSON.stringify(payload)).toString("base64");
    };


}


