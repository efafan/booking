import {ObjectID, ObjectId} from "mongodb";
import {ServiceConfiguration} from "@juicecommerce/juicecommerce/lib/core/decorators/ServiceConfiguration";
import {Juice} from "@juicecommerce/juicecommerce/lib/Juice";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {LoggingService} from "@juicecommerce/juicecommerce/lib/core/log/LoggingService";
import {Wallet} from "./models/Wallet";
import {KeyStore} from "./models/KeyStore";
import {CryptoUtils} from "./tools/CryptoUtils";
import {TransferLog} from "./models/TransferLog";

let rp = require('request-promise');

@ServiceConfiguration({
    name: "Juicechain",
    key: "juicecommerce:juicechain",
    description: "Service connecting Juicechain API",
    options: {
        juicechain: {
            node: "demo",
            url: "http://demo.juicechain.org",
            user: "none",
            password: "none"
        }
    }
})
export class JuicechainService{

    options:any;


    @Inject("juicecommerce:logging")
    private logging: LoggingService;

    constructor() {}


   /* async manage(element: ObjectId | Asset): Promise<ManagedAsset> {
        const managed = new ManagedAsset();
        await managed.load(element);
        return managed;
    }
    */

    getNodeInfo(){
        const name = this.options.juicechain.node;
        return {
            name: name
        };
    }

    async getJWT():Promise<string>{
        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + '/auth',
            headers: {
                'authorization': 'none'
            },
            body: {
                username: this.options.juicechain.user,
                password: this.options.juicechain.password
            },
            json: true
        };

        try {
            let result = await rp(options);
            if (result && result.success){
                return result.token;
            }else return null;
        }
        catch (e) {
            return e;
        }

    }



    /**
     * Fetch assets of given wallet
     * @param address
     * @param privateKey
     * TODO Change authToken to JWT
     */
    async getBalance(address: string, privateKey?: string): Promise<Array<any>>{

        //  const authToken = await CryptoUtils.generateAuthToken(privateKey, address);
        const token = await this.getJWT();
        const minconf = (this.options.juicechain.minconf != null) ? this.options.juicechain.minconf : 1;

        const options = {
            method: 'GET',
            uri: this.options.juicechain.url + "/wallet/" + address + "/" + minconf + "/" + (new Date()).toString(),
            headers: {
                "authorization": token
            },
            body: {
            },
            json: true
        };

        try {
            let result = await rp(options);
            if (result && result.success){
                return result.payload;
            } else {
                return [];
            }
        } catch (exception){
            return exception;
        }

    }


    /**
     * Transfer Asset
     *
     * @param senderAddress
     * @param senderPrivateKey
     * @param receiverAddress
     * @param asset
     * @param quantity
     * @param payload
     */
    async transfer(senderAddress: string,
                   senderPrivateKey: string,
                   receiverAddress: string,
                   asset: string, quantity: number, payload: any): Promise<any>{

        const authToken = await CryptoUtils.generateAuthToken(senderPrivateKey, senderAddress);

        quantity = parseInt(<any>quantity);

        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + "/wallet/transfer/" + receiverAddress,
            headers: {
                "authorization": authToken
            },
            body: {
                asset: asset,
                amount: quantity,
                payload: payload
            },
            json: true
        };

        try {
            let result = await rp(options);

            // generate log
            result.log = await TransferLog.log(senderAddress, receiverAddress, asset,
                quantity, payload, result.payload, result.success);

            return result;
        } catch (exception){
            return exception;
        }

    }

    public async generateAuthorizationToken(privateKey: string, publicKey: string){
        return await CryptoUtils.generateAuthToken(privateKey, publicKey);
    }

    public async loadKey(address: string, reference: string, reference_id: ObjectID):Promise<string | null>{

        const query = {
            address: address,
            reference: reference,
            reference_id: reference_id
        };

        const key = await KeyStore.findOne<KeyStore>(query);

        return (key) ? key.privateKey : null;
    }


    /**TODO JWT authorization
     */
    async createWallet():Promise<Wallet>{

        const token = await this.getJWT();

        const options = {
            method: 'POST',
            uri: this.options.juicechain.url + "/wallet/",
            headers: {
                "authorization": token
            },
            body: {
            },
            json: true
        };

        try {
            let result = await rp(options);
            if (result && result.success){
                return <Wallet>{
                    address: result.payload.address,
                    privateKey: result.payload.privateKey,
                    publicKey: result.payload.publicKey,
                    node: result.payload.node,
                    provider: result.payload.provider,
                    title: result.payload.title
                }
            }
            return null;
        } catch (exception){
            return exception;
        }

    }

    public async storeKey(address: string, privateKey: string,
                          reference: string, reference_id: ObjectID): Promise<boolean>{

        const keyStore = new KeyStore();
        keyStore.address = address;
        keyStore.privateKey = privateKey;

        keyStore.reference = reference;
        keyStore.reference_id = reference_id;

        await keyStore.save();

        return true;
    }

    /**
     * Get specific amount of assets currently available in an wallet
     * @param address
     * @param assetName
     */
    public async getAssetBalance(address: string, privateKey: string, assetName: string):Promise<number>{

        let balance = await this.getBalance(address, privateKey);

        let amount = 0;
        balance.forEach(asset => {
            if (asset.name == assetName){
                amount = asset.quantity;
            }
        });

        return amount;
    }


    async onUpdate(options: any): Promise<boolean> {
        this.options = options;
        return true;
    }

    getNodeUrl(){
        return this.options.juicechain.url;
    }

    async onConfigure(): Promise<boolean> {
        return undefined;
    }

    // TODO: Fetch token
    async onStartup(): Promise<boolean> {
        this.logging.debug("JuicEchain", "Node: " + this.options.juicechain.node + " (" + this.options.juicechain.url + ")");
        return true;
    }

}