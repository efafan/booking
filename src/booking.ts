import {ApplicationConfiguration} from "@juicecommerce/juicecommerce/lib/core/decorators/ApplicationConfiguration";
import {IJuiceApplication} from "@juicecommerce/juicecommerce/lib/core/IJuiceApplication";
import {ColoredConsoleLogger} from "@juicecommerce/juicecommerce/lib/core/log/ColoredConsoleLogger";
import {Juice} from "@juicecommerce/juicecommerce/lib/Juice";
import {NetworkingService} from "@juicecommerce/juicecommerce/lib/services/networking/NetworkingService";
import {BookingAppRoute} from "./BookingAppRoute";
import {JuicechainService} from "./service/juicechain/JuicechainService";
import {AssetService} from "./service/assets/AssetService";

@ApplicationConfiguration({
    key: "juicecommerce:tgm",
    options: {
        logger: "logger:colored-console"
    }
})

export class booking implements IJuiceApplication{
    constructor(){

    }

    async configure(): Promise<boolean> {
        return undefined;
    }

    async prepare(): Promise<boolean> {

        Juice.register(ColoredConsoleLogger);

        await Juice.deployService(NetworkingService);
        await Juice.deployService(JuicechainService);
        await Juice.deployService(AssetService);



        return undefined;
    }

    async ready(): Promise<any> {
        let networking = Juice.service<NetworkingService>("juicecommerce:networking");


        //booking route
        let bookingApp= new BookingAppRoute();
        await networking.deployRoute("/booking", bookingApp);


        console.log('Ready');
    }



}