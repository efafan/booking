import {IRouteHandler} from "@juicecommerce/juicecommerce/lib/services/networking/routes/IRouteHandler";
import {IComponent} from "@juicecommerce/juicecommerce/lib/core/component/IComponent";
import {Inject} from "@juicecommerce/juicecommerce/lib/core/decorators/Inject";
import {NetworkingService} from "@juicecommerce/juicecommerce/lib/services/networking/NetworkingService";
import {ExpressRouter} from "sunset-express-rc/lib/ExpressRouter";
import cors = require("cors");
import {TestController} from "./service/test/TestController";
import {AuthenticationController} from "@juicecommerce/juicecommerce/lib/services/juicebox/controller/AuthenticationController";
import {InquiresController} from "./service/inquires/InquiresController";
import {CustomerController} from "./service/customers/CustomerController";
import {AuthController} from "@juicecommerce/juicecommerce/lib/modules/management/AuthController";
import {UserController} from "./service/users/UserController";
import {AssetController} from "./service/assets/AssetsController";

export class BookingAppRoute implements IRouteHandler, IComponent{

    @Inject("juicecommerce:networking")
    private networking: NetworkingService;

    private router: ExpressRouter;
    private testController;
    private assetController;
    private inquiresController;
    private customerController;
    private userController;

    constructor(){
        this.router = new ExpressRouter();
        this.router.use(cors());


        this.router.use(function (req, res, next) {
           // console.log(req.originalUrl);
            //return res.send({success:true});
            next();
        })
    }

    getRouter(): ExpressRouter {
        return this.router;
    }

    async init(): Promise<boolean> {

        // this.testController = new TestController();
       // this.authController= new AuthController();
        this.inquiresController= new InquiresController();
        this.customerController= new CustomerController();
        this.userController= new UserController();
        this.assetController= new AssetController();


        // this.router.addController("/test", this.testController);
        this.router.addController("/inquires", this.inquiresController);
        this.router.addController("/customers", this.customerController);
        this.router.addController('/', new AuthController());
        this.router.addController('/users', this.userController);
        this.router.addController('/assets', this.assetController);


        return true;
    }

}